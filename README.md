# Introdução a Ciência de Dados com Python e Orange

Este repositório contém exemplos de código para suporte às disciplinas relacionadas com Ciência de Dados na UTFPR-Curitiba. Os exemplos de python/jupyter se encontram na pasta *notebooks*. Os exemplos de Orange se encontram na pasta *orange*.

Os notebooks podem ser executados usando o Binder. Clique no link: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/introcienciadedados%2Ftutoriais/HEAD)

## Requistos e instalações (Python)

Para executar os exemplos localmente, é preciso ter instalado:

- Python 3 (ou mais atual)
- Git 
- Jupyter Notebook

O Python e o Jupyter Notebook (e diversos outros pacotes relacionados) podem ser instalados usando a distribuição Anaconda em: [https://www.anaconda.com/distribution/](https://www.anaconda.com/distribution/)

O Git pode ser obtido em: [https://git-scm.com/downloads](https://git-scm.com/downloads)

###  Download dos tutoriais e execução

Para fazer o download dos exemplos, clique neste [link](https://gitlab.com/introcienciadedados/tutoriais/-/archive/master/tutoriais-master.zip)

Para fazer o download dos exemplos usando o git, inicie uma janela de terminal e execute (Linux):

```
git clone https://gitlab.com/luizcelso/datascience.git
cd datascience/
jupyter-notebook
```

O Jupyter iniciará uma janela do navegador mostrando a lista de diretórios do projeto. Os exemplos se encontram na pasta `notebooks`.

## Requistos e instalações (Orange)

O Orange pode ser obtido em: [https://orangedatamining.com/download/](https://orangedatamining.com/download/)

###  Download dos tutoriais e execução

Para fazer o download dos exemplos, clique neste [link](https://gitlab.com/introcienciadedados/tutoriais/-/archive/master/tutoriais-master.zip)

Para abrir os exemplos, primeiro descompacte o arquivo baixado. Em seguida, abra o Orange, cliquem em `open` e navegue até o diretório onde você descompactou os tutoriais.

### Playlists de vídeos de Orange

Vários vídeos práticos sobre o Orange com base nos tutoriais podem ser encontrados nas playlists abaixo:
 
 - [Canal do Prof. Luiz Celso Gomes-Jr](https://www.youtube.com/playlist?list=PLI_WaTKy-Dj3B_6bas_gbMx0AYiEZBGxk)
 - [Canal do Prof. André Santanchè](https://www.youtube.com/playlist?list=PL3JRjVnXiTBZoCdQ_llJRt7dmxpaFqVg0)
